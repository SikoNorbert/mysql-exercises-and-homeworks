-- Write an SQL query responsible for creating the movies table which should contain columns: movie_id of type varchar with maximum length36 movie_name of type varchar with maximum length40 movie_type of type varchar with maximum length30 movie_rating of type tinyint
create table if not exists Movies(
id integer,
movieName varchar(40),
movieType varchar(30),
rating tinyint
);
create table if not exists Autor(
id integer,
numeAutor varchar(30),
prenumeAutor varchar(30),
dataNasterii date,
loculNasterii varchar(50),
genLiterar varchar(20)
);

create table if not exists Carte(
ISBN integer,
numeCarte varchar(20),
descriereCarte varchar(30),
pret double(5,2),
numarPagini integer,
edituraCarte varchar(30),
genLiterar varchar(20) 
);