create database SDA;

create table Author(
id integer,
numeAutor varchar(20),
prenumeAutor varchar(20),
loculNasterii varchar(20)
);

insert into Author values(1, 'Creanga', 'Ion', 'Botosani');
insert into Author values(1, 'Aron', 'Pumnu', 'Iasi');
insert into Author values(2, 'Creanga', 'Ion', 'Botosani');

select * from Author;
select numeAutor, loculNasterii from Author;
update Author set loculNasterii='Hoghiz' where id=1;
delete from Author where id=1;
